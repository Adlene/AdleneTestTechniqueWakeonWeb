<?php
namespace App\Calculator;

use \App\Calculator\CalculatorInterface;
use App\Model\Change;

class Mk1Calculator implements CalculatorInterface{
    public function getChange(int $amount): ?Change
    {
        if(is_int($amount)){
            $bank = new Change();
            $bank->coin1 = $amount;
            return $bank;
        }
        else{
        return null;
        }
    }
    public function getSupportedModel(): string
    {
        return "mk1";
    }
}