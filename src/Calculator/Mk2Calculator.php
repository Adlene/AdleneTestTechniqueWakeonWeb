<?php
namespace App\Calculator;

use \App\Calculator\CalculatorInterface;
use App\Model\Change;

class Mk2Calculator implements CalculatorInterface{
    public function getChange(int $amount): ?Change
    {
        $bank = new Change();
// methode plus optimale: on recupere l'entier du montant divisé par la valeur du billet pour chaque valeur (10, 5, 2)
//aide d'un formateur pour cet methode plus optimale
        $bank->bill10 = intval($amount / 10);
        $amount = $amount % 10;
        $bank->bill5 = intval($amount / 5);
        $amount = $amount % 5;
        $bank->coin2 = intval($amount / 2);
        $amount = $amount % 2;

        if ($amount !== 0)
        {
            return null;
        }
        else
            {
            return $bank;
        }

  /*correction de la première méthode, on retire chaque billet de $amount en incrementant les attributs de $bank correspondant

        while ($amount >= 10) {
            $bank->bill10++;
            $amount -= 10;
        }
        while ($amount >= 5) {
            $bank->bill5++;
            $amount -= 5;
        }
        while ($amount >= 2) {
            $bank->coin2++;
            $amount -= 2;
        }
        if ($amount !== 0)
        {
            return null;
        }
        else
        {
        return $bank;
        }

/*premiere methode utlisé (non fonctionnelle: ne passe pas les premières difficultés)

        if($amount >= 2 && is_int($amount)){
           if($amount>=10){
               while($bank->bill10 != $amount/10) {
                   $bank->bill10++;
               }
               return $bank;
           }
           elseif($amount>=5){
               while($bank->bill5 != $amount/5) {
                   $bank->bill5++;
               }
               return $bank;
           }
           elseif($amount>=2){
               while($bank->coin2 != $amount) {
                   $bank->coin2++;
               }
               return $bank;
           }
        }
        else{
            return null;
        }
*/
    }
    public function getSupportedModel(): string
    {
        return "mk2";
    }
}