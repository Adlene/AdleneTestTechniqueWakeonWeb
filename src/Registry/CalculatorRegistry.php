<?php
namespace App\Registry;

use App\Calculator\CalculatorInterface;
use App\Calculator\Mk1Calculator;
use App\Calculator\Mk2Calculator;

class CalculatorRegistry implements \App\Registry\CalculatorRegistryInterface
{
    public function getCalculatorFor(string $model): ?\App\Calculator\CalculatorInterface
    {
        //retourne la classe correspondante au modele renseigné en parametre
        if($model ="mk1")
        {
            return new Mk1Calculator();
        }
        elseif($model = "mk2")
        {
            return new Mk2Calculator();
        }
        else
            {
                return NULL;
            }
    }
}

