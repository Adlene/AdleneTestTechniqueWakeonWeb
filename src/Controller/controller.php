<?php

namespace App\Controller\controller;

use App\Calculator\Mk1Calculator;
use App\Calculator\Mk2Calculator;
use App\Service\GetChange;
/**
 * @Route("/automaton", name="automaton")
 */
class CalculatorController
{
    /**
     * @Route("/automaton/mk1/change/{$amount}", name="mk1change", method "GET")
     */
    public function getChangeMk1(Mk1Calculator $mk1Calculator, $amount)
    {
        $bank = new Mk1Calculator();
        $bank->getChange($amount);
        return $bank;
    }
    /**
     * @Route("/automaton/mk2/change/{$amount}", name="mk2change", method "GET")
     */
    public function getChangeMk2(Mk1Calculator $mk1Calculator, $amount)
    {
        $bank = new Mk2Calculator();
        $bank->getChange($amount);
        return $bank;
    }

}
